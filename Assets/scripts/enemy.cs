﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour 
{

	public float moveSpeed = 5f;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2 (transform.localScale.x * moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Wall")
		{
			Flip();
		}
	}

	void OnTriggerExit2D(Collider2D other) 
	{
		Die ();
	}

	void Die()
	{
		GetComponent<Rigidbody2D>().gravityScale = 0f;
		//Destroy (gameObject);
	}
}
