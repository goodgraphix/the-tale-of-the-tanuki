﻿using UnityEngine;
using System.Collections;

public class changeToLevel2 : MonoBehaviour {

	//to not allow
	private bool isLoading;

	// Use this for initialization
	void Start () {
		isLoading = false;
	}
	
	// Update is called once per frame
	void Update () {
		//
		if (Input.GetKeyDown ("p"))
		{
			if(!isLoading)
			{
				Application.LoadLevel("Level1");
				isLoading = true;
			}
		}
	}
}
