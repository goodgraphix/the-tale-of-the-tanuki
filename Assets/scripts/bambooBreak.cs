﻿using UnityEngine;
using System.Collections;

public class bambooBreak : MonoBehaviour {
	
	public GameObject player;
	public bool inStatue;
	public MovePlayer script;
	public GameObject breakChunk;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D (Collision2D collider) 
	{
		Vector2 relvel = collider.relativeVelocity;
		float vThing = relvel.magnitude;
		inStatue = script.inStatueMode;
		if(collider.gameObject.tag == "Player")
		{
			if(inStatue == true)
			{
				if(vThing > 10f)
				{

					this.Break(relvel);

				}
			}
		}
	}

	void makeBamboo(Vector2 relvel) {
		for(int i = -1; i < 4; i++)
		{
			for(int j = -1; j < 4; j++)
			{
				GameObject newThing = Instantiate(breakChunk,new Vector2(GetComponent<Rigidbody2D>().position.x + i/2, GetComponent<Rigidbody2D>().position.y + j/2), Quaternion.identity) as GameObject;
				newThing.GetComponent<BambooFragment>().setRotation(-90f);
				newThing.GetComponent<BambooFragment>().GetComponent<Rigidbody2D>().AddForce(new Vector2( (relvel.x / -40) + Random.Range(-1,3), (relvel.y / -40) + Random.Range(-1,10)));

			}
		}
		
	}

	void Break (Vector2 relvel)
	{
		makeBamboo(relvel);
		Destroy (gameObject);//particle break system insert here ADAM
	}
}
