﻿using UnityEngine;
using System.Collections;

public class changeLevelFromTitle : MonoBehaviour {

	//to not allow
	private bool isLoading;

	// Use this for initialization
	void Start () {
		isLoading = false;
	}
	
	// Update is called once per frame
	void Update () {
		//
		if (Input.GetKeyDown ("space"))
		{
			if(!isLoading)
			{
				Application.LoadLevel("mainscene");
				isLoading = true;
			}
		}
		if (Input.GetKeyDown ("e")) {
			Application.Quit ();
		}
	}
}
