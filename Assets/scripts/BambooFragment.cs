﻿using UnityEngine;
using System.Collections;

public class BambooFragment : MonoBehaviour {
	int decayTimer;
	public Sprite[] bambooSprites;
	private SpriteRenderer sprRend;

	// Use this for initialization
	void Start () {
		bambooSprites = Resources.LoadAll<Sprite>("Sprites/bamboo_pieces_grid_80_300");
		int bambooSpriteNum = Random.Range (0, bambooSprites.Length);
		decayTimer = 60 * Random.Range(2,5);
		sprRend = GetComponent<SpriteRenderer> ();
		sprRend.sprite = bambooSprites[bambooSpriteNum];
	}
	
	// Update is called once per frame
	void Update () {

		--decayTimer;
		if (decayTimer <= 0) {
			Destroy(gameObject);
			return;
		}
		if (decayTimer <= 50) {
			sprRend.color = new Color(sprRend.color.r,sprRend.color.g,sprRend.color.b,(float)decayTimer/50);
		}
	}

	public void setRotation(float degree) {
		GetComponent<Rigidbody2D>().rotation = degree;
	}
}
