﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {

	public float walkSpeed;
	public float runSpeed;
	public float speed;
	public float torque;
	public bool facingRight = true;
	public Transform groundCheck;
	public bool isGrounded = false;
	public LayerMask whatIsGrounded;
	public float jumpForce;
	public float transForce;
	public float playerHeight = 0.25f;
	public bool inStatueMode = false;
	public Sprite tanukiSprite;
	public Sprite statueSprite;

	//new stuff from 
	public float groundRadius = 0.2f;

	private int CONTROL_DIR = 0;

	private bool CONTROL_RUN = false;
	private bool CONTROL_JUMP = false;
	private bool CONTROL_TRANSFORM = false;
//	private int COOLDOWN_JUMP = 0;
//	private int COOLDOWN_TRANSFORM = 0;

	private Rigidbody2D rigidBody;
	private BoxCollider2D maBox;

	public PhysicsMaterial2D softMat;
	public PhysicsMaterial2D hardMat;

	private SpriteRenderer sprRend;

	// Use this for initialization
	void Start () {
		sprRend = GetComponent<SpriteRenderer> ();
		tanukiSprite = Resources.Load<Sprite>("Sprites/testnuki2");
		statueSprite = Resources.Load<Sprite> ("Sprites/stoneNuki");

		rigidBody = GetComponent<Rigidbody2D>();
		maBox = GetComponent<BoxCollider2D> ();
	}

	void FixedUpdate () {

		//capture inputs and save them
		CONTROL_DIR = Input.GetAxis ("Horizontal") < 0 ? -1 : 0;
		if (Input.GetAxis ("Horizontal") > 0) CONTROL_DIR += 1;

		CONTROL_RUN = Input.GetButton("Dash");

		CONTROL_JUMP = Input.GetButtonDown ("Jump");

		CONTROL_TRANSFORM = Input.GetButtonDown("Transform");

		isGrounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, 1 << LayerMask.NameToLayer("Ground"));

		if (!inStatueMode) {
			// directional movement
			rigidBody.velocity = new Vector3 ( (CONTROL_RUN ? runSpeed : walkSpeed) * CONTROL_DIR, rigidBody.velocity.y);
			//flip the sprite/animation
			if ((CONTROL_DIR == 1 && !facingRight) || (CONTROL_DIR == -1 && facingRight)) Flip ();
		}

		if (CONTROL_TRANSFORM) {
			ChangeStatueMode();
		}

//		if (CONTROL_TRANSFORM && COOLDOWN_TRANSFORM == 0) {
//			COOLDOWN_TRANSFORM = 15;
//			ChangeStatueMode();
//		} 
//		if (COOLDOWN_TRANSFORM > 0) {
//			COOLDOWN_TRANSFORM--;
//		}
	}

	// Update is called once per frame
	void Update () {
		if (!inStatueMode) {
			sprRend.sprite = tanukiSprite;
			if (isGrounded && CONTROL_JUMP) {
				Jump();
			}
//			if (isGrounded && CONTROL_JUMP && COOLDOWN_JUMP == 0) {
//				COOLDOWN_JUMP = 15;
//				Jump();
//			} 
//			if (COOLDOWN_JUMP > 0) {
//				COOLDOWN_JUMP--;
//			}
		} else {
			sprRend.sprite = statueSprite;	
		}
	}
	
	void Jump() {
		Debug.Log("JUMP HANDLER");
		CONTROL_JUMP = false;
		rigidBody.AddForce(new Vector2(0,jumpForce));
	}
	
	void Flip() {
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void ChangeStatueMode() {
		Debug.Log("TRANSFORM HANDLER "+Time.time);
		CONTROL_TRANSFORM = false;
		maBox.enabled = false;
		rigidBody.fixedAngle = false;
		if(!inStatueMode) {
			//STATUE MODE
			maBox.sharedMaterial = hardMat;
			inStatueMode = true;
			rigidBody.AddForce (new Vector3(rigidBody.velocity.x * transForce, rigidBody.velocity.y * transForce));

			if(facingRight)
				rigidBody.AddTorque(-1 * torque);
			else
				rigidBody.AddTorque(torque);
		} else {
			//MAMMAL MODE
			maBox.sharedMaterial = softMat;
			inStatueMode = false;
			rigidBody.rotation = 0.0f;
			rigidBody.fixedAngle = true;
		}
		maBox.enabled = true;
	}



}
