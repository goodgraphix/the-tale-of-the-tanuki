﻿using UnityEngine;
using System.Collections;

public class breaking : MonoBehaviour {

	public GameObject player;
	public bool inStatue;
	public MovePlayer script;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D (Collision2D collider) 
	{
		inStatue = script.inStatueMode;
		if(collider.gameObject.tag == "Player")
		{
			if(inStatue == true)
			{
				if(player.GetComponent<Rigidbody2D>().velocity.x > 5f)
				{
					//replace with custom breaking stuff
					this.GetComponent<AudioSource>().Play ();
				}
			}

		}
	}
}
